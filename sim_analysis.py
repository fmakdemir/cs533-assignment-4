#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pdb
import numpy as np
import argparse
from sklearn.metrics.pairwise import cosine_similarity
from futils import *
from stemmers import *

parser = argparse.ArgumentParser(description='IR - Homework 4 - Similarity Analysis')
parser.add_argument('--num-docs', '-n', dest='num_docs', type=int, help='Number of Documents', default=1000)
parser.add_argument('--stemmer', '-s', dest='stemmer', type=str, choices=STEMMER_KEYS, help='Stemmer for tokenized words', default=STEMMER_KEYS[0])
parser.add_argument('--clustering', '-c', dest='clustering', type=str, choices=['c3m', 'kmeans'], help='Clustering Method', default='c3m')
parser.add_argument('--binarize', '-b', dest='binarize', action='store_true', help='Binarize D Matrix')
parser.add_argument('--similarity-metric', '-m', dest='similarity', choices=['dice', 'inner-product', 'cosine'], help='Binarize D Matrix', default='dice')
parser.add_argument('--debug', '-d', dest='debug', action='store_true', help='Enable debug mode')
parser.add_argument('--test', '-t', dest='test', action='store_true', help='Run on test data')

params = parse_args(parser)

keys, D = read_csv('d_mat-' + params.stemmer)

doc_ids = D[:,0]
D = D[:params.num_docs,1:]

if params.binarize: D = np.array(D > 0, dtype=int)

if params.test:
    D = np.array([
        [0, 0, 0, 1, 1],
        [1, 1, 0, 1, 0],
        [1, 1, 1, 1, 0],
        [0, 1, 0, 1, 1],
        ])
    D = np.array([
        [1, 0, 1, 1, 1],
        [1, 1, 0, 1, 0],
        ])
    D = np.array([
        [2, 0, 1, 3, 2],
        [1, 0, 2, 1, 5],
        ])

S = calculate_sim(D, params.similarity)
print('Similarity matrix:')
print(S)
save_np(S, 'sim-%04d-s=%s-b=%s-m=%s' % (params.num_docs, params.stemmer, params.binarize, params.similarity))

_, labels = read_csv('%s-res-%04d-b=%s-s=%s' % (params.clustering, params.num_docs, params.binarize, params.stemmer))

cm = {} # cluster map containing list of docs for each label

for r in labels:
    doc_id, label = r[0], r[1]
    if label not in cm: cm[label] = [doc_id]
    else: cm[label].append(doc_id)

print('Clusters:', cm)

print('Cluster sizes:')
print('%4s %4s %10s' % ('cid', 'size', 'intra sim.'))
print(S)
LC = len(cm) # get num of clusters
c_summ = np.zeros((LC, 3)) # cluster summary
means = np.zeros((LC, D.shape[1] + 1))
sum_sim = 0
sum_non_sing_sim = 0
num_non_sing_sim = 0
for i, k in enumerate(cm):
    c = cm[k]
    avg_sim = np.average(S[c][:, c])
    summ = (k, len(c), avg_sim)
    c_summ[i, :] = summ
    means[i][0] = k
    means[i][1:] = np.average(D[c], axis=0) # get means as centroids
    print('%4d %4d %6.3f' % summ)
    sum_sim += avg_sim
    if summ[1] > 1:
        sum_non_sing_sim += avg_sim
        num_non_sing_sim += 1

print('\nAvg. intra cluster similarity: %.4f' % (sum_sim / LC))
print('Avg. intra non singular cluster similarity: %.4f\n' % (sum_non_sing_sim / num_non_sing_sim))

save_np(c_summ, 'cluster-summary-%04d-s=%s-b=%s-m=%s' % (params.num_docs, params.stemmer, params.binarize, params.similarity))
save_np(means, 'means-%04d-s=%s-b=%s-m=%s' % (params.num_docs, params.stemmer, params.binarize, params.similarity))

print('Inter class similarities:')
Sm = calculate_sim(means, params.similarity)

print(Sm)

print('\n\nAvg. inter cluster similarity: %.4f' % np.average(Sm))
