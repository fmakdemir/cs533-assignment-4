#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import os
import xml.etree.ElementTree as ET
import argparse
import pdb
import numpy as np
from nltk.tokenize import RegexpTokenizer
from futils import *
from stemmers import *

parser = argparse.ArgumentParser(description='IR - Homework 4 - Preprocess')
parser.add_argument('--num-docs', '-n', dest='num_docs', type=int, help='Number of Documents', default=1000)
parser.add_argument('--stemmer', '-s', dest='stemmer', type=str, choices=STEMMER_KEYS, help='Stemmer for tokenized words', default=STEMMER_KEYS[0])
parser.add_argument('--debug', '-d', dest='debug', action='store_true', help='Enable debug mode')

params = parse_args(parser)

stemmer_func = STEMMERS[params.stemmer]

stopwords = set([w.strip() for w in open('stopwords.txt').readlines() if w != '\n'])
print('Number of stopwords:', len(stopwords))

custom_stopwords = ['nin', 'in', 'nun', 'un', 'i', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'o']
for w in custom_stopwords:
    stopwords.add(w)

tokenizer = RegexpTokenizer(r'[\w\'’]+')

terms = {}
words = []
not_stemmed = set()
stop_word_cnt = 0
print('{:<6} {:<6} {:<9} {:<6}'.format('terms', 'words', 'stopwords', 'not stemmed'))

ti = 0
D = []
for i in range(params.num_docs):
    tree = ET.parse('./MilliyetCollectionTREC/{}.txt'.format(i))
    root = tree.getroot()
    title = root.findall('./HEADLINE')[0].text.lower()
    content = root.findall('./TEXT')[0].text.lower()

    text_tokenized = tokenizer.tokenize(title + ' ' + content)

    filtered_text = []

    for w in text_tokenized:
        if w not in stopwords:
            not_stemmed.add(w)
            w = stemmer_func(w)
            filtered_text.append(w)
            words.append(w)
            if w not in terms:
                terms[w] = 1
                ti += 1
            else: terms[w] += 1
        else: stop_word_cnt += 1
    D.append(filtered_text)
    if (i+1) % 100 == 0:
        print('{:6} {:6} {:9} {:6}'.format(len(terms), len(words), stop_word_cnt, len(not_stemmed)))

D_dict = []
len_D = len(D)
for i, d in enumerate(D):
    print('\rCalculating row: {:5}/{}'.format(i + 1, len_D), end='')
    unique, counts = np.unique(d, return_counts=True)
    row = dict(zip(unique, counts))
    row['doc_id'] = i
    D_dict.append(row)
save_dict_rows_as_csv(D_dict, 'd_mat-' + params.stemmer, ['doc_id', *terms.keys()])

st = sorted([(k, v) for (k, v) in terms.items()], key=lambda x: -x[1])
print('%5s %s' % ('words', 'frequency'))
print('\n'.join('%5s %3d' % wf for wf in st[:10]))
if params.debug: pdb.set_trace()
