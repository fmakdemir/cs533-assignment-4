#!/usr/bin/env bash
STEMMERS='f5 porter'
METHODS='c3m kmeans'

echo '====Running preprocess...===='
for s in $STEMMERS; do python3 preprocess.py -s $s || break; done
echo '====Running clustering methods...===='
for m in $METHODS; do
    for s in f5 porter; do
        for n in $(seq 100 100 1000); do
            python3 $m.py -s $s -n $n || break 3
        done
    done
done
echo '====Running Similarity Analysis...===='
for m in $METHODS; do
    for s in $STEMMERS; do
        for n in $(seq 100 100 1000); do
            python3 sim_analysis.py -c $m -s $s -n $n || break 3
        done
    done
done
