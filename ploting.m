close all;

% m_x = [ terms, words, stopwords, not stemmed ]
% each row shows values for 100, 200, ..., 1000 documents
m_f5 = ...
[5603  22472      3392  10124;
 8599  42256      5818  17041;
 10336  59338      8467  22058;
 12351  80688     11576  27633;
 13221  96443     13733  30621;
 14765 118940     17051  35411;
 15830 137689     19244  38754;
 16852 156573     22127  42409;
 17795 176467     24816  45670;
 18486 191756     26967  48064 ] ;

m_porter = ...
[10062  22472      3392  10124;
 16915  42256      5818  17041;
 21878  59338      8467  22058;
 27395  80688     11576  27633;
 30347  96443     13733  30621;
 35075 118940     17051  35411;
 38377 137689     19244  38754;
 41985 156573     22127  42409;
 45196 176467     24816  45670;
 47555 191756     26967  48064 ];

% document count
d = 100:100:1000;

figure;
plot (d, m_f5(:,1), '-x'); hold on;
plot (d, m_porter(:,1), '-o')
xlim ([0,1100]);
xlabel('Number of documents')
ylabel('Stemmed terms')
l=legend ('f5', 'porter');
title(l, 'Stemmer')
l.Location = 'northwest';

figure;
plot (d, (m_porter(:,3)./m_porter(:,2)*100));
xlabel('Number of documents')
ylabel('Stopword %')
xlim ([0,1100]);
