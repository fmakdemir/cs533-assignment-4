from nltk.stem import PorterStemmer

PS = PorterStemmer()

def stemmer_f5(w):
    return w[:5]

def stemmer_porter(w):
    return PS.stem(w)

STEMMERS = { 'f5': stemmer_f5, 'porter': stemmer_porter }
STEMMER_KEYS = [k for k in STEMMERS]
