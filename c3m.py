#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import os
import argparse
import pdb
import numpy as np
from time import time
from futils import *
from stemmers import *

parser = argparse.ArgumentParser(description='IR - Homework 4 - C3M')
parser.add_argument('--num-docs', '-n', dest='num_docs', type=int, help='Number of Documents', default=1000)
parser.add_argument('--stemmer', '-s', dest='stemmer', type=str, choices=STEMMER_KEYS, help='Stemmer for tokenized words', default=STEMMER_KEYS[0])
parser.add_argument('--binarize', '-b', dest='binarize', action='store_true', help='Binarize D Matrix')
parser.add_argument('--debug', '-d', dest='debug', action='store_true', help='Enable debug mode')
parser.add_argument('--test', '-t', dest='test', action='store_true', help='Run on test data')
parser.add_argument('--seed-method', '-m', dest='seed', type=str, choices=['ne', 'nc'], help='Seed count use for nc real (nc) or estimation (ne)', default='nc')

params = parse_args(parser)

col_keys, data = read_csv('d_mat-' + params.stemmer)
D = data[:params.num_docs,1:]

if params.binarize: D = np.array(D > 0, dtype=int)

if params.test:
    D = np.array([[0, 0, 0, 1, 1],
       [1, 1, 0, 1, 0],
       [1, 1, 1, 1, 0],
       [0, 1, 0, 1, 1]])

start_time = time()

# calculate A and B row/col summation matrices
A = np.sum(D, axis=1)
A[A == 0] = 1 # to prevent div by zero
B = np.sum(D, axis=0)
B[B == 0] = 1 # to prevent div by zero

# calculate S and S'
Sa = (D.T / A).T
Sb = D / B

C = Sa @ Sb.T # calculate C dissimilarity matrix

save_np(C, 'c_mat-%04d-b=%s-s=%s' % (params.num_docs, params.binarize, params.stemmer)) # save C

diag = np.diag(C) # get diagonals

# calculate ne and nc
ne_f = D.shape[0] * D.shape[1] / np.sum(D > 0)
ne = int(np.round(ne_f))

nc_f = np.sum(diag)
nc = int(np.round(nc_f))

sc = nc if params.seed == 'nc' else ne

# calculate seed powers and get ne seeds with highest power
P = diag * (1 - diag) * A

Pm = sorted([(i, p) for i, p in enumerate(P)], key=lambda v: v[1])[:-sc-1:-1]
seeds = np.array([p[1] for p in Pm])
sids = np.array([p[0] for p in Pm])

# calculate clusters
res = sids[np.argmax(C[:, sids], axis=1)]

print('Seed powers:', P)
print('Selected seeds:', Pm)
print('Seed cluster: mismatch:', np.sum(res[sids] != sids))
print('ne = {:.06} ~= {}'.format(ne_f, ne))
print('nc = {:.06} ~= {}'.format(nc_f, nc))
print('Elapsed time:', time() - start_time)

res[sids] = sids # make sure seeds are assigned to themselves in any case
save_clusters(res, 'c3m-res-%04d-b=%s-s=%s' % (params.num_docs, params.binarize, params.stemmer))
if params.debug: pdb.set_trace()
