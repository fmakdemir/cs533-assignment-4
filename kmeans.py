#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pdb
import numpy as np
import argparse
from sklearn.cluster import KMeans
from sklearn.metrics import silhouette_score as ss
from time import time
from futils import *
from stemmers import *

parser = argparse.ArgumentParser(description='IR - Homework 4 - KMeans')
parser.add_argument('--num-docs', '-n', dest='num_docs', type=int, help='Number of Documents', default=1000)
parser.add_argument('--stemmer', '-s', dest='stemmer', type=str, choices=STEMMER_KEYS, help='Stemmer for tokenized words', default=STEMMER_KEYS[0])
parser.add_argument('--binarize', '-b', dest='binarize', action='store_true', help='Binarize D Matrix')
parser.add_argument('--debug', '-d', dest='debug', action='store_true', help='Enable debug mode')
parser.add_argument('--test', '-t', dest='test', action='store_true', help='Run on test data')

params = parse_args(parser)

keys, d = read_csv('d_mat-' + params.stemmer)

docID = d[:,0]
d = d[:params.num_docs,1:]

if params.binarize: d = np.array(d > 0, dtype=int)

if params.test:
    D = np.array([[0, 0, 0, 1, 1],
       [1, 1, 0, 1, 0],
       [1, 1, 1, 1, 0],
       [0, 1, 0, 1, 1]])

start_time = time()

t = np.count_nonzero(d)
n = d.shape[0]
m = d.shape[1]

nc_f = m * n / t
nc = int(np.round(nc_f))
print('nc:', nc)
if nc > n:
    nc = int(np.sqrt(n))
    print('NC too big chosing as sqrt(n). nc:', nc)
kmeans = KMeans(n_clusters=nc, random_state=0).fit(d)
print('Model fit:', kmeans.labels_)
# print('Silhoutte Score:', ss(d, kmeans.labels_))
print('Elapsed time:', time() - start_time)

save_clusters(kmeans.labels_, 'kmeans-res-%04d-b=%s-s=%s' % (params.num_docs, params.binarize, params.stemmer))
