CS 553 - IRS
============

## Required Libraries

See requirements.txt

C3M is implemented using the following paper
* Can, F., Ozkarahan, E. A. (1990). Concepts and effectiveness of the cover-coefficient-based clustering methodology for text databases. ACM Transactions on Database Systems 15 (4): 483. doi:10.1145/99935.99938

There are 3 stages.
In order to see usage run related stagex.py file such as `./stage1.py -h`

Both stopword and document set are courtesy of given references.

# Stage 1
Includes analysis of first 1000 documents of Milliyet TREC document set.

# Stage 2
First define common variables used by runs:
```bash
export STEMMERS='f5 porter'
export METHODS='c3m kmeans'
```

Then run preprocessors to produce data required by later steps

```bash
for s in $STEMMERS; do python3 preprocess.py || break; done
```

Then run c3m and kmeans
```bash
for m in $METHODS; do
    for s in f5 porter; do
        for n in $(seq 100 100 1000); do
            python3 $m.py -s $s -n $n || break 3
        done
    done
done
```

Then run similariy analysis of clusters:
```bash
for m in $METHODS; do
    for s in $STEMMERS; do
        for n in $(seq 100 100 1000); do
            python3 sim_analysis.py -c $m -s $s -n $n || break 3
        done
    done
done
```


`break n` statement break nested loops so you can use cancel to break out of all loops.

# Stage 3

# References
* Can, F., Kocberber, S., Balcik, E., Kaynak, C., Ocalan, H. C., & Vursavas, O. M. (2008). Information retrieval on Turkish texts. Journal of the American Society for Information Science and Technology, 59(3), 407-421.
