import tempfile
import gzip
import csv
import os
import numpy as np
import shutil
import sys
from sklearn.metrics.pairwise import cosine_similarity

CSV_DIR = 'csv'
NPY_DIR = 'npy'
LOG_DIR = 'logs'

FP_LIST = ['debug', 'test', 'prog', 'data_dir']

# overwrite print
_print = print

params = {}
outfile = None
def set_params(p):
    global params, outfile
    params = p
    p_dict = vars(params).items()
    # get log path with params
    path = get_log_path(params.prog + '-' + '-'.join('{}={}'.format(*v) for v in p_dict if v[0] not in FP_LIST))
    print('Opening log file:', path)
    outfile = open(path, 'w')

def print(*args, **kwargs):
    _print(*args, **kwargs)
    if outfile:
        _print(*args, **kwargs, file=outfile)
        outfile.flush()
    sys.stdout.flush()

def print_params(params):
    set_params(params)
    max_arg_len = max(len(k) for k in vars(params))
    arg_format = '{:' + str(max_arg_len) + 's}: {}'
    print('Running params:\n\t', '\n\t'.join(arg_format.format(v[0], v[1]) for v in vars(params).items()), sep='')

def parse_args(parser):
    params = parser.parse_args()
    params.prog = parser.prog
    print_params(params)
    return params

def get_csv_path(name):
    if not os.path.exists(CSV_DIR):
        print('CSV_DIR not found trying to create...')
        os.makedirs(CSV_DIR)
        print('CSV_DIR created on path:', CSV_DIR)
    return os.path.join(CSV_DIR, name + '.csv.gz')

def get_log_path(name):
    if not os.path.exists(LOG_DIR):
        print('LOG_DIR not found trying to create...')
        os.makedirs(LOG_DIR)
        print('LOG_DIR created on path:', LOG_DIR)
    return os.path.join(LOG_DIR, name + '.log')

def get_np_path(name):
    if not os.path.exists(NPY_DIR):
        print('NPY_DIR not found trying to create...')
        os.makedirs(NPY_DIR)
        print('NPY_DIR created on path:', NPY_DIR)
    return os.path.join(NPY_DIR, name + '.npy.gz')

def check_file_exists(path):
    if not os.path.exists(path):
        print('File not found on path:', path)
        print('Please make sure to run previous steps!!!\n\n')

def save_dict_rows_as_csv(drows, name, col_keys):
    path = get_csv_path(name)
    with tempfile.SpooledTemporaryFile(mode='w') as tmp_f:
        writer = csv.DictWriter(tmp_f, col_keys, restval=0)
        writer.writeheader()
        len_rows = len(drows)
        print('\nSaving data')
        for i, r in enumerate(drows):
            print('\rWriting row: {:5}/{}'.format(i + 1, len_rows), end='')
            writer.writerow(r)
        tmp_f.seek(0)
        with gzip.open(path, 'wt') as gz_out:
            shutil.copyfileobj(tmp_f, gz_out)
        print('\nSaved to file:', path)

def read_csv(name):
    path = get_csv_path(name)
    check_file_exists(path)
    with gzip.open(path, 'rt') as f:
        rows = [r for r in csv.reader(f)]
        keys = rows[0]
        return keys, np.array(rows[1:], dtype=np.int)

def save_np(data, name):
    tmp_path = '.tmp-' + name
    np.savetxt(tmp_path, data)
    path = get_np_path(name)
    with open(tmp_path, 'rb') as tmp_f:
        with gzip.open(path, 'wb') as gz_out:
            shutil.copyfileobj(tmp_f, gz_out)
    os.unlink(tmp_path)
    print('\nSaved to file:', path)

def load_np(name, dtype=None):
    tmp_path = '.tmp-' + name
    path = get_np_path(name)
    check_file_exists(path)
    with gzip.open(path, 'rt') as gz_in:
        with open(tmp_path, 'wt') as tmp_f:
            tmp_f.write(gz_in.read())
    data = np.loadtxt(tmp_path, dtype=dtype)
    os.unlink(tmp_path)
    print('\nLoaded from file:', path)
    return data

def save_clusters(labels, name):
    label_d = []
    for i, r in enumerate(labels):
        label_d.append({
            'doc_id': i,
            'cluster_id': labels[i],
        })
    save_dict_rows_as_csv(label_d, name, ['doc_id', 'cluster_id'])

def calculate_sim(D, sim='dice'):
    # library method is better optimized
    if sim == 'cosine': return cosine_similarity(D)

    I = D @ D.T # get inner product for later use
    if sim == 'inner-product': return I

    L2 = np.sum(D ** 2, axis=1) # feature length squares
    if sim == 'dice':
        DT = np.tile(L2, (L2.shape[0], 1)) # tiled D^2
        return 2 * I / (DT + DT.T)

    # in case we want to use our own version of cosine sim. calc.
    L = L2.reshape(-1, 1) ** 0.5 # feature vector lengths
    if sim == 'cosine': return I / (L @ L.T)
    raise TypeError('Similarity type not implemented: ' + sim)
