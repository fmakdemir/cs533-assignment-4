#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import os
import xml.etree.ElementTree as ET
from nltk.tokenize import RegexpTokenizer
from nltk.stem import PorterStemmer
import argparse
import pdb
import numpy as np
from futils import *
# https://stackoverflow.com/a/533638
from whoosh import index
from whoosh.fields import Schema, TEXT, ID
from whoosh.analysis import CharsetFilter, StemmingAnalyzer
from whoosh.support.charset import accent_map
import whoosh.query

PS = PorterStemmer()

def stemmer_f5(w):
    return w[:5]

def stemmer_porter(w):
    return PS.stem(w)

STEMMERS = { 'f5': stemmer_f5, 'porter': stemmer_porter }
STEMMER_KEYS = [k for k in STEMMERS]

parser = argparse.ArgumentParser(description='IR - Homework 4')
parser.add_argument('--num-docs', '-n', dest='num_docs', type=int, help='Number of Documents', default=1000)
parser.add_argument('--stemmer', '-s', dest='stemmer', type=str, choices=STEMMER_KEYS, help='Stemmer for tokenized words', default=STEMMER_KEYS[0])
parser.add_argument('--data-dir', '-t', dest='data_dir', type=str, help='Data Directory', default='data')
parser.add_argument('--debug', '-d', dest='debug', action='store_true', help='Enable debug mode')
parser.add_argument('--write-index', '-w', dest='write_index', action='store_true', help='Writes Indexing result to whoosh')

params = parse_args(parser)

params.stemmer = STEMMERS[params.stemmer]

stopwords = set([w.strip() for w in open('stopwords.txt').readlines() if w != '\n'])
print('Number of stopwords:', len(stopwords))

custom_stopwords = ['nin', 'in', 'nun', 'un', 'i', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'o']
for w in custom_stopwords:
    stopwords.add(w)

tokenizer = RegexpTokenizer(r'[\w\'’]+')

if not os.path.exists(params.data_dir):
    print('Data directory not found creating:', params.data_dir)
    os.makedirs(params.data_dir)

# stem and convert accent chars to ascii ones
if params.write_index:
    analyzer = StemmingAnalyzer(stoplist=stopwords) | CharsetFilter(accent_map)
    schema = Schema(title=TEXT(analyzer=analyzer), content=TEXT(analyzer=analyzer), id=ID)

    ix = index.create_in(params.data_dir, schema)
    ix = index.open_dir(params.data_dir)
    writer = ix.writer()

terms = {}
words = []
not_stemmed = set()
stop_word_cnt = 0
print('{:<6} {:<6} {:<9} {:<6}'.format('terms', 'words', 'stopwords', 'not stemmed'))
# https://whoosh.readthedocs.io/en/latest/stemming.html
ti = 0
D = []
for i in range(params.num_docs):
    tree = ET.parse('./MilliyetCollectionTREC/{}.txt'.format(i))
    root = tree.getroot()
    title = root.findall('./HEADLINE')[0].text.lower()
    content = root.findall('./TEXT')[0].text.lower()
    if params.write_index:
        writer.add_document(title=title, content=content, id=str(i))

    text_tokenized = tokenizer.tokenize(content)

    filtered_text = []

    for w in text_tokenized:
        if w not in stopwords:
            not_stemmed.add(w)
            w = params.stemmer(w)
            filtered_text.append(w)
            words.append(w)
            if w not in terms:
                terms[w] = 1
                ti += 1
            else: terms[w] += 1
        else: stop_word_cnt += 1
    D.append(filtered_text)
    if (i+1) % 100 == 0:
        print('{:6} {:6} {:9} {:6}'.format(len(terms), len(words), stop_word_cnt, len(not_stemmed)))
if params.write_index: writer.commit()

D_dict = []
len_D = len(D)
for i, d in enumerate(D):
    print('\rCalculating row: {:5}/{}'.format(i + 1, len_D), end='')
    unique, counts = np.unique(d, return_counts=True)
    row = dict(zip(unique, counts))
    row['doc_id'] = i
    D_dict.append(row)
save_dict_rows_as_csv(D_dict, 'd_mat.csv', ['doc_id', *terms.keys()])

if params.write_index:
    with ix.searcher() as searcher:
        print(searcher.document(id='0'))
        print(searcher.search(whoosh.query.Every()))

st = sorted([(k, v) for (k, v) in terms.items()], key=lambda x: -x[1])
print('%5s %s' % ('words', 'frequency'))
print('\n'.join('%5s %3d' % wf for wf in st[:10]))
if params.debug: pdb.set_trace()
